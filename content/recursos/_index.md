---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 5
title: Recursos
created: 1332707586
date: 2012-03-25
aliases:
- "/node/5/"
- "/page/5/"
---

## Software Livre

* [Como contribuir para projectos de software livre]({{< ref "/recursos/como-contribuir" >}})

## Administração Pública

* [Administração Pública]({{< ref "/recursos/ap" >}})
* [Agenda Digital]({{< ref "/recursos/agenda-digital" >}})
* [Concursos Públicos]({{< ref "/recursos/concursos-publicos" >}})
* [Manifesto do Campo das Cebolas]({{< ref "/recursos/campo-das-cebolas" >}})
* [Monitorização do RNID]({{< ref "/iniciativas/monitorizacao-rnid" >}})


## Direito de Autor

* [Direito de Autor]({{< ref "/recursos/da" >}})
* [ACTA]({{< ref "/recursos/acta" >}})
* [Cópia Privada]({{< ref "/recursos/copia-privada" >}})
  * [Apresentação à CECC]({{< ref "/recursos/copia-privada-prescecc20120208" >}})
  * [Consulta Pública Europeia de 2012]({{< ref "/recursos/copia-privada-consulta-publica-2012" >}})
* [DRM]({{< ref "/iniciativas/diz-nao-ao-drm" >}})
* [EUCD]({{< ref "/recursos/eucd" >}})

## Outros

* [Patentes]({{< ref "/recursos/swpat" >}})
* [Legislação]({{< ref "/recursos/legislacao" >}})
* [15 de Outubro]({{< ref "/recursos/15O" >}})
* [Comércio Electrónico](https://github.com/marado/ecommerce-WTO/blob/master/ecommerce.md)
* [Comunidades Tecnológicas Portuguesas](https://github.com/zorkpt/comunidades-tech-portugal)

