---
categories:
- newsletter
metadata:
  tags:
  - tags_tid: 330
  node_id: 787
layout: article
title: Newsletter 2021 - 1° trimestre
created: 1618437198
date: 2021-04-14
aliases:
- "/article/787/"
- "/node/787/"
---
<p>No 1º trimestre de 2021 a ANSOL desenvolveu um conjunto de acções e actividades, em várias áreas, e cumpriu algumas tradições próprias desta altura do ano.</p><ul><li><p>Como tem vindo a ser habitual, a ANSOL abriu o ano com a celebração do Dia do Domínio Público, como podem ler <a href="https://ansol.org/dominio-publico-2021" title="https://ansol.org/dominio-publico-2021" rel="noopener noreferrer nofollow">neste artigo</a>;</p></li><li><p>Escrevemos também sobre <a href="https://ansol.org/RNID2020" title="https://ansol.org/RNID2020" rel="noopener noreferrer nofollow">como decorreu 2020</a> no que diz respeito à Lei das Normas Abertas;</p></li><li><p>Ainda sobre a Lei das Normas Abertas, demos uma <a href="https://shifter.sapo.pt/2021/02/lei-das-normas-abertas/" title="https://shifter.sapo.pt/2021/02/lei-das-normas-abertas/" rel="noopener noreferrer nofollow">entrevista ao Shifter</a>;</p></li><li><p>Cumprindo a tradição, estivemos presentes na <a href="https://openforumeurope.org/event/policy-summit-2021/" title="https://openforumeurope.org/event/policy-summit-2021/" rel="noopener noreferrer nofollow">EU Open Source Policy Summit 2021</a>;</p></li><li><p>No dia do amor ao Software Livre, <a href="https://ansol.org/ilovefs2021" title="https://ansol.org/ilovefs2021" rel="noopener noreferrer nofollow">fizemos a nossa declaração</a>;</p></li><li><p>Aconteceu em Fevereiro mais uma edição da <a href="https://fosdem.org/2021/" title="https://fosdem.org/2021/" rel="noopener noreferrer nofollow">FOSDEM</a>, deste vez em formato não presencial, mas a ANSOL tomou parte, com tem sido hábito nos últimos anos, de vários eventos da FOSDEM e pré-FOSDEM;</p></li><li><p>Assinámos, juntamente com mais de 100 outras entidades e 150 académicos, um <a href="https://www.communia-association.org/2021/03/22/communia-supports-the-wto-trips-waiver-for-covid-19/" title="https://www.communia-association.org/2021/03/22/communia-supports-the-wto-trips-waiver-for-covid-19/" rel="noopener noreferrer nofollow">pedido à Organização Mundial do Comércio</a> para suspender temporariamente as regras à "propriedade intelectual" nos casos necessários para o apoio à prevenção, contenção e tratamento do COVID-19;</p></li><li><p>O artigo "<a href="https://www.esop.pt/destaque/zoom-out-sobre-o-caracter-semi-publico-dos-eventos-publicos">Zoom out: sobre o carácter semi-público dos eventos públicos</a>" publicado pela ESOP e com o qual a ANSOL tem total concordância, foi divulgado nos seus canais de comunicação habituais potenciado a sua visibilidade junto de todos os que nos seguem.</p></li></ul><h2>Novos sócios:</h2><ul><li><p>P. Grave</p></li><li><p>R. Sellani</p></li><li><p>M. Sarmento</p></li><li><p>D. Valente</p></li></ul>
