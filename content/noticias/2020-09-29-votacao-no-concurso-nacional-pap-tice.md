---
categories: []
metadata:
  image:
  - image_fid: 58
    image_alt: ''
    image_title: ''
    image_width: 850
    image_height: 600
  node_id: 752
layout: article
title: Votação no Concurso Nacional PAP TICe
created: 1601416982
date: 2020-09-29
aliases:
- "/article/752/"
- "/node/752/"
---
<p data-pm-slice="1 1 []">A votação pelo público nos projetos das Provas de Aptidão Profissional do concurso nacional PAP TICe está aberta até <strong>25 de setembro, às 23h:59m</strong>.</p><p>A inciativa da <a href="http://www.anpri.pt/" title="http://www.anpri.pt/" rel="noopener noreferrer nofollow">Associação Nacional de Professores de Informática</a>, que tem o apoio institucional da ANSOL, contou com projetos em várias categorias: Software, Artefactos (Hardware, Redes, Robótica e Eletrónica), e Multimédia, entre os quais se encontram projetos livres.</p><p>Para informação sobre os projetos e como votar podem consultar o <a href="http://www.anpri.pt/pap/" title="http://www.anpri.pt/pap/" rel="noopener noreferrer nofollow">site do concurso</a>.</p>
