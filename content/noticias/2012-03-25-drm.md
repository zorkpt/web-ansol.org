---
categories: []
metadata:
  node_id: 7
layout: page
title: DRM
created: 1332708583
date: 2012-03-25
aliases:
- "/node/7/"
- "/page/7/"
- "/politica/drm/"
---
<p>A <a href="http://en.wikipedia.org/wiki/Copyright_Directive">diretiva europeia 2001/29/CE</a>, que a ANSOL acompanhou, resultou na lei <a href="http://www.dre.pt/util/getpdf.asp?s=dip&amp;serie=1&amp;iddr=2004.199A&amp;iddip=20042861">50/2004</a>. Entre outros problemas, d&aacute; for&ccedil;a legal ao DRM.</p>
<p>A ANSOL tem-se debatido contra o DRM, e tem um site de campanha em <a href="http://drm-pt.info/">http://drm-pt.info/</a></p>
