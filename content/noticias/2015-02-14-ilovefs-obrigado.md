---
categories:
- ansol
- "#ilovefs"
- fsfe
- drupal
- apache
- debian
- httpd
- civicrm
- mailman
- openstreetmap
- mapnik
metadata:
  tags:
  - tags_tid: 33
  - tags_tid: 62
  - tags_tid: 63
  - tags_tid: 64
  - tags_tid: 65
  - tags_tid: 66
  - tags_tid: 67
  - tags_tid: 68
  - tags_tid: 69
  - tags_tid: 70
  - tags_tid: 71
  node_id: 284
layout: article
title: "#ILoveFS - Obrigado!"
created: 1423936408
date: 2015-02-14
aliases:
- "/article/284/"
- "/node/284/"
---
Hoje é o dia de mostrarmos a nossa paixão pelo Software Livre. Inúmeras pessoas em todo o mundo já o estão a fazer, e a ANSOL <a href="https://ansol.org/ilovefs2015">incentiva a que se junte a elas</a>.

<a href="https://ansol.org/ilovefs2015"><img src="https://ansol.org/attachments/ilovefs-hashtag.png" alt="I love Free Software!" style="border: 0 !important;"></a>

Mas não só as pessoas em nome individual que aproveitam este dia para mostar a sua paixão pelo Software Livre:</p><blockquote class="twitter-tweet" lang="en"><p>Thank you <a href="https://twitter.com/hashtag/Linux?src=hash">#Linux</a> community for the most awesome operating system kernel <a href="https://twitter.com/hashtag/ilovefs?src=hash">#ilovefs</a></p>— openSUSE (@openSUSE) <a href="https://twitter.com/openSUSE/status/566570111595999232">February 14, 2015</a></blockquote><p>Empresas, comunidades, projectos, associações, vários organismos têm usado este dia para mostrar o seu agradecimento público àqueles que fazem o mundo do Software Livre tão vibrante como é. Assim, também a ANSOL decide aproveitar este dia para fazer alguns agradecimentos públicos.</p><h2>Software que usamos</h2><p>Manter uma associação implica a necessidade de um conjunto de recursos técnicos, mais ou menos elaborados. O uso de várias peças de tecnologia por parte da ANSOL tem variado e evoluido ao longo dos tempos, mas hoje em dia estas são algumas das peças mais importantes que temos usado:</p><ul><ul><li><strong>Debian</strong>: Os vários servidores usados pela ANSOL, alojados graças ao apoio da <a href="http://www.netureza.pt/">Netureza</a>, que aproveitamos também aqui para agradecer, estão a correr a distribuição de <a href="https://www.gnu.org/gnu/linux-and-gnu.pt-br.html">GNU/Linux</a> chamada <a href="https://www.debian.org/index.pt.html">Debian</a>. Reconhecida pela sua segurança e estabilidade, esta distribuição tem-nos potenciado chegar até à comunidade. Por exemplo, se está a ler este texto no nosso sítio web, ele está a ser servido por uma instância a correr Debian. Já agora: sabia que existe uma comunidade de utilizadores de <a href="http://svn.debianpt.org/">Debian em Portugal</a>?</li><li><strong>Apache httpd</strong>: A maior parte da comunicação entre a ANSOL e o público é feito através dos seus sites. Acessíveis com ligações seguras (graças ao patrocínio de certificados da <a href="https://www.multicert.com/pt/">Multicert</a>, a quem agradecemos também), estes sites estão a usar o servidor web <a href="http://httpd.apache.org/">Apache httpd</a>, o mais popular servidor web há quase vinte anos. Estabilidade e fácil configurabilidade são duas das razões que nos levam a manter por esta opção.</li></ul><li style="list-style-type: none;"><ul><li><strong>Drupal: </strong>E os sites propriamente ditos? Cada vez mais temos migrado <a href="https://ansol.org">os</a> <a href="https://membros.ansol.org">nossos</a> <a href="http://c.ansol.org">sites</a> para o uso da plataforma <a href="http://drupal.org">Drupal</a>. Os casos mais recentes foi o site da campanha <a href="http://nao.quero.imposto.ms/">contra o imposto Microsoft</a>. O Drupal tem uma comunidade forte e activa, e a ANSOL agradece a todos, em particular à <a href="http://drupal-pt.org/">Associação Drupal Portugal</a>, que deu corpo a essa comunidade no nosso país.</li><li><strong>CiviCRM</strong>: Mas a gestão de uma Associação precisa também de actividades mais burocráticas, como por exemplo gestão de sócios e quotas. O <a href="https://civicrm.org/">CiviCRM</a> é uma ferramenta altamente recomendada para esse tipo de gestão - e usado por Associações de todas as dimensões. Uma das grandes vantagens que encontrámos recentemente na continuação do uso desta ferramenta, é a sua <a href="https://www.drupal.org/project/civicrm">integração com o Drupal</a>, o que nos facilita muito as tarefas do dia-a-dia quanto à gestão de sócios, membros e apoiantes.</li><li><strong>Mailman</strong>: Outra pequena integração que fizemos foi entre o CiviCRM e o <a href="http://www.gnu.org/software/mailman/">Mailman</a> - um sistema popular de gestão de listas de correio electrónico, que a ANSOL vem usado desde a sua formação. É aqui que muita da nossa comunicação com sócios ou com a comunidade acontece. Um directório das listas de correio da ANSOL <a href="http://listas.ansol.org/mailman/listinfo/">pode ser vista aqui</a>, esteja à vontade para se juntar a alguma, ou ler os arquivos.</li><li><strong>OpenStreetMap</strong>: Uma interessante integração que temos também no nosso site principal é na popular secção de eventos: para muitos deles temos informação detalhada da localização graças ao <a href="http://wiki.openstreetmap.org/wiki/Mapnik">Mapnik</a>, um módulo de integração com o <a href="http://www.openstreetmap.org/">OpenStreetMap</a>. Um exemplo desta integração pode ser vista <a href="https://ansol.org/node/275">nesta página de evento</a>.</li></ul></li></ul><p>Estes são apenas alguns exemplos do software que utilizamos, e a quem dirigimos o nosso muito grande OBRIGADO! Mas queremos também agradecer a toda a comunidade de Software Livre no geral, e lançar-vos o desafio: que tal aproveitarem o dia de hoje para, também vocês, <a href="https://ansol.org/node/275">agradecerem àqueles que fazem o Software Livre que tanto adoram</a>?</p><p>&nbsp;</p>
