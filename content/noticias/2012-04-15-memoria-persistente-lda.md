---
categories:
- consultoria
- distribuição/venda
- suporte
metadata:
  email:
  - email_email: geral@m16e.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 8
  - servicos_tid: 2
  site:
  - site_url: http://www.m16e.com
    site_title: http://www.m16e.com
    site_attributes: a:0:{}
  node_id: 48
layout: servicos
title: Memória Persistente, Lda.
created: 1334500776
date: 2012-04-15
aliases:
- "/node/48/"
- "/servicos/48/"
---
<h2>
	Consultoria</h2>
<p>Estudo e implementa&ccedil;&atilde;o de solu&ccedil;&otilde;es de gest&atilde;o empresarial. Optimiza&ccedil;&atilde;o dos recursos existentes (reaproveitamento da hardware antigo). Solu&ccedil;&otilde;es escal&aacute;veis e adpat&aacute;veis a cada perfil de neg&oacute;cio.</p>
<h2>
	Distribui&ccedil;&atilde;o/Venda</h2>
<p>Comercializa&ccedil;&atilde;o de computadores com GNU/Linux pr&eacute;-instalado. Op&ccedil;&otilde;es de garantia sobre o software instalado.</p>
<h2>
	Suporte</h2>
<p>Desenvolvimento de software. Instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o de sistemas e redes baseados em GNU/Linux.</p>
