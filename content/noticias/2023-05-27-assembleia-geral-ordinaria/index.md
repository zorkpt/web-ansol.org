---
categories:
- assembleia geral
layout: article
title: Assembleia Geral 2023 Ordinária
date: 2023-05-27
---

A 29 de abril de 2023 decorreu a Assembleia Geral Ordinária da ANSOL, na
Universidade de Aveiro, com a seguinte ordem de trabalhos:

1. Apresentação e votação do relatório e contas de 2022;
2. Apresentação e votação do orçamento e plano de atividades para 2023;
3. Outros assuntos.

Os documentos apresentados estão disponíveis na nossa página de [Documentação e
Transparência](/transparencia).
