---
categories:
- imprensa
metadata:
  tags:
  - tags_tid: 19
  node_id: 349
layout: article
title: Anúncios a Software Proprietário removidos de 1125 sítios web da AP
created: 1444772251
date: 2015-10-13
aliases:
- "/article/349/"
- "/campanha-pdf/"
- "/node/349/"
---
<p>A campanha sobre leitores de PDF, organizada pela Free Software Foundation Europe -- organização da qual a ANSOL é associada -- chegou agora ao seu término, após seis anos de actividade.</p><p>Vários sítios web da Administração Pública dos vários países Europeus faziam anúncio a leitores proprietários de PDF. Em seis anos, foi feito um trabalho de levantamento destes casos, e posterior contacto com as entidades responsáveis de cada um deles no sentido de corrigir esta situação, com um sucesso superior a 50% dos casos. Uma petição sobre o assunto recolheu o apoio de 90 organizações e milhares de empresas e indivíduos.</p><p>Dando a FSFE como encerrada esta campanha, a ANSOL quer não só dar os parabéns pelo sucesso da campanha, como um agradecimento a todos os intervenientes, desde os activistas que fizeram com que a campanha fosse possível às várias entidades pela Europa fora que ouviram as nossas questões e melhoraram a situação. "Esta campanha foi um sucesso, e prova que muitas vezes a melhor forma que há de conseguir melhorar a Administração Pública é contribuindo positivamente com sugestões, e disponibilidade para ajudar a construir soluções", diz Marcos Marado, presidente da ANSOL.</p><p>Destacam-se também a eficácia da campanha em terras Lusas: de quinze casos identificados, apenas seis subsistem. "De Câmaras Municiais ao Portal das Finanças, é ainda tempo de resolver a situação" comenta Marcos Marado, apelando às instituições que ainda estão nesta situação a corrigirem de vez o problema.</p><p>Os sítios web Portugueses ainda afectados são: Câmara Municipal de Coimbra, Direcção Geral do Ensino Superior, Direcção Geral dos Impostos, Fundação para a Ciência e a Tecnologia, Instituto Camões e Universidade de Coimbra.</p><p>Mais informação sobre a campanha: http://fsfe.org/news/2015/news-20151013-01.pt.html</p><p>Sobre a FSFE: A Fundação Europeia para o Software Livre é uma organização dedicada à promoção do Software Livre que trabalha pela liberdade na sociedade da informação. -- https://fsfe.org/index.pt.html</p><p>Sobre a ANSOL: A Associação Nacional para o Software Livre é uma associação portuguesa sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento, investigação e estudo da Informática Livre e das suas repercussões sociais, políticas, filosóficas, culturais, técnicas e científicas. -- https://ansol.org</p>
