---
layout: evento
title: Encontro Ubuntu-pt Março @ Sintra
metadata:
  event:
    location: Avenida Movimento das Forças Armadas 5, 2710-436 Sintra, Portugal
    site:
      url: https://loco.ubuntu.com/events/ubuntu-pt/4316-encontro-ubuntu-pt-mar%C3%A7o-sintra/
    date:
      start: 2023-03-16 20:00:00.000000000 +01:00
      finish: 2023-03-16 23:00:00.000000000 +01:00
---

[![Cartaz](https://ansol.org/eventos/2023-03-16-ubuntu-pt-marco-sintra/cover.png)](https://loco.ubuntu.com/events/ubuntu-pt/4316-encontro-ubuntu-pt-mar%C3%A7o-sintra/)

Encontro da Comunidade Ubuntu-pt de Março de 2023, em Sintra.

Vem conhecer a comunidade, conversar sobre Ubuntu, Software Livre e tecnologia em ambiente de convívio descontraído, e traz um amigo também.

Dia 16 de Março a partir das 20h no Bar Saloon, em Sintra.
Detalhes e inscrição (não obrigatória) na [página do evento](https://loco.ubuntu.com/events/ubuntu-pt/4316-encontro-ubuntu-pt-mar%C3%A7o-sintra/).
