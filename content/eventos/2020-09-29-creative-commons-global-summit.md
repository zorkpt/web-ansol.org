---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 751
  event:
    location: Online
    site:
      title: Creative Commons Global Summit
      url: https://creativecommons.org/2020/09/25/registration-is-open-cc-global-summit/
    date:
      start: 2020-10-19 00:00:00.000000000 +01:00
      finish: 2020-10-23 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Creative Commons Global Summit
created: 1601391275
date: 2020-09-29
aliases:
- "/evento/751/"
- "/node/751/"
---
<div id="2020-10-22" class="sched-container-header"><div class="sched-container-dates"><strong>A ANSOL irá também participar neste evento, com uma apresentação 5ª feira, dia 22, às 10:30, intitulada "Public Domain in Portugal".<br></strong></div>Mais informação em <a href="https://sched.co/ef5a">https://sched.co/ef5a .</a></div>
