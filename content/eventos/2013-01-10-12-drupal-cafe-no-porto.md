---
categories: []
metadata:
  node_id: 123
  event:
    location: Bar Fuzelhas, Matosinhos
    site:
      title: ''
      url: http://groups.drupal.org/node/274788
    date:
      start: 2013-01-19 15:00:00.000000000 +00:00
      finish: 2013-01-19 18:00:00.000000000 +00:00
    map: {}
layout: evento
title: 12º Drupal Café no Porto
created: 1357839508
date: 2013-01-10
aliases:
- "/evento/123/"
- "/node/123/"
---
<p>&nbsp;</p>
<p>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
</p>
<p style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; font-size: 13px; margin-top: 0px; margin-right: 0px; margin-bottom: 1.385em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; background-position: initial initial; background-repeat: initial initial; "><b>Morada:</b><br />
	Bar Fuzelhas<br />
	Av. da Liberdade, 997 - Praia do Fuzelhas<br />
	4450-718 Le&ccedil;a da Palmeira, Matosinhos</p>
<p style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; font-size: 13px; margin-top: 0px; margin-right: 0px; margin-bottom: 1.385em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; background-position: initial initial; background-repeat: initial initial; "><strong>Como chegar:</strong></p>
<p style="border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; font-size: 13px; margin-top: 0px; margin-right: 0px; margin-bottom: 1.385em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; vertical-align: baseline; background-position: initial initial; background-repeat: initial initial; ">Esta&ccedil;&atilde;o Metro do Mercado (linha azul) + 20m a p&eacute;. Tem liga&ccedil;&atilde;o com autocarros</p>
