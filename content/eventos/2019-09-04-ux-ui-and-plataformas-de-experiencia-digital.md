---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 694
  event:
    location: 
    site:
      title: ''
      url: https://www.esop.pt/destaque/workshop-open-source-transformacao-digital-1asessao-2019
    date:
      start: 2019-10-22 00:00:00.000000000 +01:00
      finish: 2019-10-22 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: UX/UI & plataformas de Experiência Digital
created: 1567633996
date: 2019-09-04
aliases:
- "/evento/694/"
- "/node/694/"
---
<div class="clearfix text-formatted field field--name-field-cc-text-columns field--type-text-long field--label-hidden field__items"><div class="field__item"><p>A ESOP vai dar inicio a mais um Ciclo de workshops organizado em parceria com a AMA e TicApp.<br> A 1ª sessão de 2019 vai ser dedicada ao tema "UX/UI &amp; plataformas de Experiência Digital" e acontecerá a 22 de Outubro, no grande auditório do Laboratório Nacional de Engenharia Civil em Lisboa.</p></div></div>
