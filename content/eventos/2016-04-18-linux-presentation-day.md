---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 415
  event:
    location: Aveiro
    site:
      title: ''
      url: http://linux-presentation-day.pt/#
    date:
      start: 2016-04-30 00:00:00.000000000 +01:00
      finish: 2016-04-30 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Linux Presentation Day
created: 1460979252
date: 2016-04-18
aliases:
- "/LPD2016/"
- "/evento/415/"
- "/node/415/"
---
<p>Linux Presentation Day - Uma chance para conhecer o GNU/Linux e o software livre!</p>
