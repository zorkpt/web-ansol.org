---
categories:
- java
- user group
- software
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 55
  - tags_tid: 158
  - tags_tid: 159
  node_id: 382
  event:
    location: Instituto Superior Técnico - Alameda - TagusPark
    site:
      title: 23ºEncontro pt.jug
      url: http://jug.pt/2015/11/22/inscricoes-encontro-23/
    date:
      start: 2015-12-03 18:45:00.000000000 +00:00
      finish: 2015-12-03 18:45:00.000000000 +00:00
    map: {}
layout: evento
title: 23º encontro PT.JUG
created: 1448197336
date: 2015-11-22
aliases:
- "/evento/382/"
- "/node/382/"
---
<p><span><span style="font-family: Arial, Helvetica, sans-serif;">Estão abertas as inscrições para o 23º encontro do PT.JUG, marcado para o próximo dia 3 de Dezemb</span>ro, no IST – Alameda (a sala será anunciada em breve)<span style="font-family: Arial, Helvetica, sans-serif;">.</span></span><br><br><span>A agenda será a seguinte:</span></p><ul><li>18h50 – Boas vindas</li><li><span style="font-family: Arial, Helvetica, sans-serif;">19h10 –&nbsp;</span>Flame Graphs, uma (boa) alternativa para profiling de apps Java</li><li>20h00 – Intervalo</li><li><span style="font-family: Arial, Helvetica, sans-serif;">20h10 –&nbsp;</span>Jigsaw – A JDK Modular</li><li>21h00 – Jantar e networking</li></ul><div>A inscrição no evento pode ser feita no&nbsp;<a href="http://ptjug-meetup-23.eventbrite.com/?aff=ptjuggeral" target="_blank" rel="nofollow">Eventbrite</a>&nbsp;ou acompanhe no&nbsp;<a href="http://lanyrd.com/cfxrty" target="_blank">Lanyrd</a>.<br>Para mais detalhes podem, como já é habitual, consultar o nosso site&nbsp;<a href="http://jug.pt/" target="_blank" rel="nofollow">jug.pt</a>.<br><br>No final do encontro será sorteada uma licença de&nbsp;<a href="http://www.jetbrains.com/idea" target="_blank" rel="nofollow">IntelliJ IDEA</a>.<br><br>Contamos com o vosso apoio na divulgação do evento partilhando-o internamente na vossa empresa / universidade.</div><p>&nbsp;</p>
