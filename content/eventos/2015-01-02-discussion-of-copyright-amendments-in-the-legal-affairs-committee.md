---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 261
  event:
    location: Parlamento Europeu - Committee on Legal Affairs
    site:
      title: ''
      url: http://www.europarl.europa.eu/news/en/news-room/content/20150220IPR24128/html/Committee-on-Legal-Affairs-meeting-23-02-2015-15001730
    date:
      start: 2015-02-23 15:00:00.000000000 +00:00
      finish: 2015-02-23 17:30:00.000000000 +00:00
    map: {}
layout: evento
title: Discussion of Copyright amendments in the Legal Affairs Committee
created: 1420196368
date: 2015-01-02
aliases:
- "/evento/261/"
- "/node/261/"
---

