---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 346
  event:
    location: Pavilhão Jardim do UPTEC PINC, Porto
    site:
      title: ''
      url: http://www.transparenciahackday.org/2015/10/proximo-date-with-data-10-de-outubro/
    date:
      start: 2015-10-10 10:00:00.000000000 +01:00
      finish: 2015-10-10 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: Date With Data
created: 1443968500
date: 2015-10-04
aliases:
- "/evento/346/"
- "/node/346/"
---
<p>Já estamos habituados às formas convencionais de visualização de dados, como gráficos de barras, diagramas circulares ou infografias; o que mais poderíamos imaginar para traduzir visualmente a informação?</p><p>E que abordagens menos convencionais podemos congeminar a partir da informação pública? Já temos datasets de <a href="https://github.com/centraldedados/parlamento-deputados" target="_blank">deputados</a>, <a href="http://centraldedados.pt/eleicoes-legislativas/" target="_blank">eleições</a>, <a href="http://centraldedados.pt/parlamento-datas_sessoes/" target="_blank">sessões da AR</a> e <a href="http://centraldedados.pt/nomes_proprios/" target="_blank">nomes próprios</a>; e à disposição também temos os <a href="http://censos.ine.pt/" target="_blank">censos</a>, <a href="http://publicos.pt" target="_blank">contratos públicos</a>, o <a href="http://dre.tretas.org">Diário da República</a> e o <a href="http://dados.gov.pt">dados.gov.pt</a>. Como os podemos tornar visíveis, legíveis, e sobretudo interessantes?</p><p>Pensar fora da caixa, com menos PC e mais papel e caneta, é o desafio do próximo Date With Data.</p>
