---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 231
  event:
    location: Clube ISCTE, Lisboa
    site:
      title: ''
      url: https://www.facebook.com/events/1470259126575886/
    date:
      start: 2014-10-11 09:30:00.000000000 +01:00
      finish: 2014-10-11 13:00:00.000000000 +01:00
    map: {}
layout: evento
title: Ubuntu Open Day
created: 1411675801
date: 2014-09-25
aliases:
- "/evento/231/"
- "/node/231/"
---
<p><span class="fsl">Ubuntu Portugal, o ISCTE-Instituto Universitário de Lisboa e o MOSS – Mestrado em Software de Código Aberto, organizam no próximo dia 11 de Outubro o Evento “Ubuntu – First Class”. Um encontro que tem por objetivo a divulgação do Sistema Operativo Ubuntu. É aberto ao público em geral e dirigido a todos os interessados independentemente dos seus conhecimentos prévios. Este evento decorrerá no Clube ISCTE entre as 9h30 e as 13h00. Não é necessário ter conhecimentos prévios sobre Ubuntu. Os participantes agrupar-se-ão nos grupos de acordo com os seus interesses.<br> <br> Este evento tem o seguinte programa:<br> </span></p><ul><li class="fsl"><span class="fsl"> 9:30 Apresentação - Carlos J. Costa, ISCTE-IUL director do Mestrado em Software de Código Aberto</span></li><li class="fsl"><span class="fsl">10:00 Overview do<span class="text_exposed_show"> Ubuntu e da sua comunidade - Ana Figueiras, Comunidade Portuguesa de Ubuntu</span></span></li><li class="fsl"><span class="fsl"><span class="text_exposed_show">10:30 Ubuntu principais funcionalidades - Tiago Carrondo, Comunidade Portuguesa de Ubuntu</span></span></li><li class="fsl"><span class="fsl"><span class="text_exposed_show">11:00 Discussão e Hands-on (grupos)</span></span></li></ul><p>O evento é organizado por:</p><ul><li class="fsl"><span class="fsl"><span class="text_exposed_show"> Comunidade Portuguesa do Ubuntu</span></span></li><li class="fsl"><span class="fsl"><span class="text_exposed_show">ISCTE-IUL ACM Students Chapter</span></span></li><li class="fsl"><span class="fsl"><span class="text_exposed_show">Mestrado em Software de Código Aberto (MOSS)<br> </span></span></li></ul>
