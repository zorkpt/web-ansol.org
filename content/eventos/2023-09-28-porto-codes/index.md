---
layout: evento
title: Porto Codes
metadata:
  event:
    location: Estambul Doner Kebap, R. de Sampaio Bruno · Porto
    site:
      url: https://www.meetup.com/portocodes/events/295454528/
    date:
      start: 2023-09-28 19:30:00.00000 +01:00
      finish: 2023-09-28 21:30:00.000 +01:00
---

Porto Codes is a meetup for local and international programming enthusiasts.
The talks are recorded and published with the presenters' consent as one of our
goals is to provide programming resources to the community.
