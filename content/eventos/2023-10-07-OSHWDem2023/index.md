---
layout: evento
title: OSHWDem2023
metadata:
  event:
    location: Museu Domus, Corunha, Galiza, Espanha
    site:
      url: https://oshwdem.org
    date:
      start: 2023-10-07
      finish: 2023-10-07
---
(pt)
O que é?

A OSHWDem (Open Source HardWare Demonstration) é uma feira de tecnologia aonde se expõe os inventos com um nexo comum: todos eles são projectos livres, ou seja, que as suas especificações, esquemas e código foram publicados, de forma que qualquer pessoa possa estudá-los, entender o seu funcionamento, replicar-os e até melhorá-los… Na OSHWDem encontrarás inventos impresionantes sobre novas tecnologías, robótica, arte, Internet das cosas, electrónica, hardware, software…

Além disto, durante a feira realizam-se competições de robótica espetaculares aonde poderás ver robots lutando de forma autónoma, resolvendo complexos labirintos de tamanho real à velocidade da luz e competindo entre eles em corridas incríveis. Podes assistir como público ou participar com o teu robot.

(es)
¿Qué es?

La OSHWDem (Open Source HardWare Demonstration) es una feria de tecnología donde se exponen inventos con un nexo común: todos ellos son proyectos libres, es decir, que sus especificaciones, esquemas y código han sido publicados, de forma que cualquiera pueda estudiarlos, entender su funcionamiento, replicarlos e incluso mejorarlos… En la OSHWDem encontrarás inventos impresionantes sobre nuevas tecnologías, robótica, arte, Internet de las cosas, electrónica, hardware, software…

Además de esto durante la feria se celebran competiciones de robótica espectaculares donde podrás ver a robots peleando de forma autónoma, resolviendo complejos laberintos de tamaño real a la velocidad de la luz y compitiendo entre ellos en carreras increíbles. Puedes asistir como publico o participar con tu robot.



![Cartaz](cartaz_oshwdem2023.png)
