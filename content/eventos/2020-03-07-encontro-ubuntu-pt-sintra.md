---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 736
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/268225079
    date:
      start: 2020-03-12 20:00:00.000000000 +00:00
      finish: 2020-03-12 20:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1583598271
date: 2020-03-07
aliases:
- "/evento/736/"
- "/node/736/"
---

