---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 409
  event:
    location: Lisboa
    site:
      title: ''
      url: http://makerfairelisbon.com
    date:
      start: 2016-06-25 00:00:00.000000000 +01:00
      finish: 2016-06-26 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Lisbon Maker Faire
created: 1459766272
date: 2016-04-04
aliases:
- "/evento/409/"
- "/node/409/"
---

