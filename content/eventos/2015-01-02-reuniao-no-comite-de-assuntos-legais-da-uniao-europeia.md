---
categories:
- ttip
- propriedade intelectual
- europa
- eu
- ue
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  - tags_tid: 93
  - tags_tid: 35
  - tags_tid: 94
  - tags_tid: 95
  node_id: 262
  event:
    location: 
    site:
      title: ''
      url: http://www.europarl.europa.eu/sides/getDoc.do?type=COMPARL&reference=JURI-OJ-20150416-1&language=EN
    date:
      start: 2015-04-16 00:00:00.000000000 +01:00
      finish: 2015-04-16 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Reunião no Comité de Assuntos Legais da União Europeia
created: 1420196427
date: 2015-01-02
aliases:
- "/evento/262/"
- "/node/262/"
---
<p style="text-indent: -35.4pt; margin-left: 35.4pt; margin-right: 0pt; margin-top: 12pt; margin-bottom: 0pt;">&nbsp;</p><p style="text-indent: -35.4pt; margin-left: 35.4pt; margin-right: 0pt; margin-top: 12pt; margin-bottom: 0pt;">Items na agenda proposta, de relevância para a actividade da ANSOL:</p><p style="text-indent: -35.4pt; margin-left: 35.4pt; margin-right: 0pt; margin-top: 12pt; margin-bottom: 0pt;">8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recommendations to the European Commission on the negotiations for the Transatlantic Trade and Investment Partnership (TTIP)</p><p style="text-indent: -35.4pt; margin-left: 35.4pt; margin-right: 0pt; margin-top: 12pt; margin-bottom: 0pt;">15.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Towards a renewed consensus on the enforcement of Intellectual Property Rights: An EU Action Plan</p>
