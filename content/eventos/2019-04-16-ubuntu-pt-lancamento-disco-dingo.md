---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 667
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/257002212
    date:
      start: 2019-04-18 00:00:00.000000000 +01:00
      finish: 2019-04-18 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Ubuntu-PT: Lançamento Disco Dingo'
created: 1555429898
date: 2019-04-16
aliases:
- "/evento/667/"
- "/node/667/"
---

