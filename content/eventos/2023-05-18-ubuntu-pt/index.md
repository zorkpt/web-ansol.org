---
layout: evento
title: Encontro da Comunidade Ubuntu Portugal
metadata:
  event:
    date:
      start: 2023-05-18 20:00:00.000000000 +01:00
      finish: 2023-05-18 23:00:00.000000000 +01:00
    location: Café Amizade, Rua da Banda Amizade N.36, Bairro do Liceu, Aveiro
    site:
      url: https://glua.ua.pt/ubuntupt-230518/
---

Mais uma vez a comunidade Ubuntu Portugal se irá reunir em Aveiro, acolhidos pelo Grupo de Linux da Universidade de Aveiro (GLUA).

Vem conviver e partilhar experiências com a comunidade e traz amigos.

O jantar não é obrigatório, podes estar a consumir outras coisas.

Para facilitar as marcações com o café pedimos que te [inscrevas](https://glua.ua.pt/ubuntupt-230518/) até dia 17.
