---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 203
  event:
    location: 
    site:
      title: ''
      url: https://www.fsf.org/events/happy-30th-birthday-gnu
    date:
      start: 2013-09-27 00:00:00.000000000 +01:00
      finish: 2013-09-27 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 30º aniversário GNU
created: 1375350589
date: 2013-08-01
aliases:
- "/evento/203/"
- "/node/203/"
---
<p>A ANSOL irá celebrar os 30 anos do GNU durante o evento do Software Freedom Day.</p><p>Mais detalhes em https://ansol.org/node/130 .</p>
