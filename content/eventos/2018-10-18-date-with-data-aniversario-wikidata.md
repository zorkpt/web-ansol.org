---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 629
  event:
    location: 
    site:
      title: ''
      url: https://www.wikidata.org/wiki/Wikidata:Sixth_Birthday/Date_With_Data_(Porto)
    date:
      start: 2018-11-03 00:00:00.000000000 +00:00
      finish: 2018-11-03 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Date With Data - Aniversário Wikidata
created: 1539887832
date: 2018-10-18
aliases:
- "/evento/629/"
- "/node/629/"
---

