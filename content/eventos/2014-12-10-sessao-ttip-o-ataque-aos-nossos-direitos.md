---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 254
  event:
    location: ISPA, Lisboa
    site:
      title: ''
      url: https://www.facebook.com/events/738860456198667/
    date:
      start: 2014-12-13 10:00:00.000000000 +00:00
      finish: 2014-12-13 13:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Sessão TTIP: o ataque aos nossos direitos'
created: 1418212809
date: 2014-12-10
aliases:
- "/evento/254/"
- "/node/254/"
---

