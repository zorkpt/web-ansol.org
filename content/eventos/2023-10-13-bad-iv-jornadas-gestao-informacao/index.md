---
layout: evento
title: IV Jornadas de Gestão da Informação
metadata:
  event:
    location: Convento São Francisco, Coimbra
    site:
      url: https://eventos.bad.pt/event/iv-jornadas-de-gestao-da-informacao-interacao-entre-arquivistas-e-informaticos/
    date:
      start: 2023-10-28 09:00:00.000000000 +01:00
      finish: 2023-10-28 17:30:00.000000000 +01:00
---

![](cartaz.png)

O  Grupo de Trabalho de Arquivos Municipais (GT-AM) da BAD, em parceria com o
Município de Coimbra,  organizam as IV Jornadas de Gestão da Informação –
Interação entre arquivistas e informáticos, que se irá realizar no próximo dia
13 de outubro,  no Convento São Francisco,  Sala D. Afonso Henriques, em
Coimbra.

Tal como nas edições anteriores, pretende-se com este evento estreitar o
diálogo entre arquivistas e informáticos, profissionais cuja colaboração se
apresenta como essencial para a gestão integrada da informação, mantendo-a
completa, fidedigna e acessível de forma continuada.

Com o tema Interoperabilidade semântica e tecnológica,  propõe-se que estas IV
Jornadas sejam mais uma oportunidade para refletir e compreender como
diferentes sistemas e linguagens tecnológicos e arquivísticos podem coexistir,
tendo a interoperabilidade enquanto requisito crítico para a eficaz gestão da
informação.
