---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 198
  event:
    location: Espaço J, Jardim da Estrada dos Arneiros, Benfica
    site:
      title: 
      url: 
    date:
      start: 2013-07-13 17:00:00.000000000 +01:00
      finish: 2013-07-13 22:00:00.000000000 +01:00
    map: {}
layout: evento
title: Festival Criar Contraste - Dança
created: 1373535131
date: 2013-07-11
aliases:
- "/cc2013-danca/"
- "/evento/198/"
- "/node/198/"
---
<p><img src="http://2.bp.blogspot.com/-6-l-4bItPr4/UdJBvPEmHaI/AAAAAAAABVU/wq7HaZ0XO_A/s320/cartazdan%C3%A7aamareloCC+(1).JPG" alt="Cartaz Festival Criar Contraste - Dança" title="Cartaz Festival Criar Contraste - Dança" width="163" height="232" style="float: left; margin-right: 1 em !important; padding: 1em;"></p><p>Sim, é um tema inesperado, mas a ANSOL vai ter uma presença discreta no próximo dia 13 de Julho a partir das 17h nesta sessão do Festival Criar Contrastes, organizado pelo Pelouro da Juventude da Junta de Freguesia de Benfica.</p><p>Estaremos lá para dar a conhecer o software livre e as comunidades nacionais a quem as quiser conhecer, com ofertas de CDs de GNU/Linux e filmes com licenças Creative Commons.</p><p>O <a href="http://clubej.blogspot.pt/2013/07/festival-criar-contraste-danca.html">programa em geral pode ser consultado aqui</a> e o local é o mesmo onde temos realizado já alguns eventos, o jardim do Eucaliptal em Benfica junto à estrada dos Arneiros.</p>
