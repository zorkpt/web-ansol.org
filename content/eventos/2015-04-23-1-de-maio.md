---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 313
  event:
    location: Alameda, Lisboa
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/manifestacao-1o-de-maio-lisboa/
    date:
      start: 2015-05-01 00:00:00.000000000 +01:00
      finish: 2015-05-01 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 1º de Maio
created: 1429828360
date: 2015-04-23
aliases:
- "/evento/313/"
- "/node/313/"
---
<p>A ANSOL junta-se às manifestações do 1º de Maio, contestando os tratados TTIP e CETA.</p>
