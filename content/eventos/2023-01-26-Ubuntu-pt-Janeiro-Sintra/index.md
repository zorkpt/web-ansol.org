---
layout: evento
title: Encontro Ubuntu-pt de Janeiro em Sintra
metadata:
  event:
    location: Bar Saloon, Avenida Movimento das Forças Armadas n 5, 2710-433 Sintra, Portugal 
    site:
      url: https://loco.ubuntu.com/events/ubuntu-pt/4309-encontro-ubuntu-pt-janeiro-sintra/
    date:
      start: 2023-01-26
      finish: 2023-01-26
---

[![Cartaz](https://ansol.org/eventos/2023-01-26-ubuntu-pt-janeiro-sintra/cover.svg)](http://www.openstreetmap.org/node/1594158358?layers=N)

Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal encontra-se no Saloon, em Sintra.

Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa.

### Mais Informações
 * [Mapa para o Bar Saloon](http://www.openstreetmap.org/node/1594158358), (2 min. a pé da estaçao de comboios da portela de Sintra)
