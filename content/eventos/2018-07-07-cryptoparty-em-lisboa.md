---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAABDVABrxkkiwBJb04AeYENA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38750930884553e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9144091934004e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9144091934004e1
    mapa_top: !ruby/object:BigDecimal 27:0.38750930884553e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9144091934004e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38750930884553e2
    mapa_geohash: eyckrymkx1nfexp3
  slide:
  - slide_value: 0
  node_id: 625
  event:
    location: "      Biblioteca dos Coruchéus Rua Alberto Oliveira 1700-019 Lisboa
      (Freguesia de Alvalade)"
    site:
      title: http://privacylx.org/events/cryptoparty/
      url: http://privacylx.org/events/cryptoparty/
    date:
      start: 2018-07-14 15:00:00.000000000 +01:00
      finish: 2018-07-14 18:30:00.000000000 +01:00
    map: {}
layout: evento
title: Cryptoparty em Lisboa
created: 1530988440
date: 2018-07-07
aliases:
- "/cryptopartylx-201807/"
- "/evento/625/"
- "/node/625/"
---
<p>Cryptoparties são workshops de formato aberto com o objetivo de proporcionar a troca de conhecimentos sobre como podemos proteger a privacidade no mundo digital.</p><p>Serão abordados tópicos como: * Tor / browsers * Segurança de comunicações * Extensões para browsers que aumentam a privacidade * motores de busca * encriptação de e-mail * etc…</p><p>A entrada é livre!</p>
