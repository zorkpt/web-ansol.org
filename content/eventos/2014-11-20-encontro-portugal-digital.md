---
excerpt: Decorrendo no edifício C6 da FCUL às 18 horas do dia 25 de Novembro, trata-se
  de um encontro pró-digital, pró-Internet e pró-modernidade a propósito da tão contestada
  proposta de Lei da Cópia Privada (PL/246); a FCUL disponibiliza espaço para um debate
  sobre o futuro dos conteúdos digitais, a necessária mercantilização  de que os consumidores
  têm estado anos à espera e a re-credibilização de uma indústria que pouco mais tem
  feito do que antagonizar os seus próprios clientes.
categories:
- internet
- streaming
- mp3
- audio
- video
- "#pl246"
- cópia privada
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 49
  - tags_tid: 50
  - tags_tid: 51
  - tags_tid: 52
  - tags_tid: 53
  - tags_tid: 45
  - tags_tid: 11
  node_id: 247
  event:
    location: Faculdade de Ciências da Universidade de Lisboa, edifício C6, Anfiteatro
      6.1.36
    site:
      title: ''
      url: https://www.fc.ul.pt/pt/evento/25-11-2014/encontro-portugal-digital
    date:
      start: 2014-11-25 18:00:00.000000000 +00:00
      finish: 2014-11-25 20:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Portugal Digital
created: 1416487591
date: 2014-11-20
aliases:
- "/evento/247/"
- "/node/247/"
- "/portugaldigital/"
---
<h3>Como Potenciar Velhos Negócios nas Novas Plataformas?</h3><p>A <em>Internet</em> apresentou-se aos consumidores, nos últimos 15 anos de forma, crescentemente atraente. Desde o início da implantação da banda larga em Portugal temos progressivamente vindo a ter acesso a ofertas de crescente velocidade, fiabilidade e&nbsp; mobilidade. Muito dificilmente alguém dos nossos dias sentirá saudades dos <em>modems</em> de 28kps, do telefone “impedido” pela Internet e do tempo em que WWW significava <em>World Wide Wait</em>.<br><br>A maior parte das indústrias percebeu o fenómeno a adaptou-se a tempo. É por isso que na Internet se transaccionam, hoje em dia, quase todo o tipo de mercadorias desde bilhetes de avião a livros (novos e usados), passando por dispositivos electrónicos,&nbsp; software as a service e muitas coisas mais. De facto, o comercio online está perfeitamente estabelecido e permite não só a aquisição dos produtos habituais, que ficaram disponíveis neste novo canal, mas também a aquisição de produtos muito especializados que dificilmente se encontrariam de outra forma em mercados pequenos, como tradicionalmente é o do nosso país.<br><br>Há no entanto um subconjunto da indústria que acordou tarde para esta realidade, se é que acordou de facto, e pretende rebater os efeitos colaterais de sua própria inacção - o discurso é o mesmo de há 15 anos - com novos impostos sobre a população, por via da taxação de diversos tipos de dispositivos electrónicos.<br><br>A propósito da tão contestada <a href="https://c.ansol.org/pl246">proposta de Lei da Cópia Privada (PL/246)</a>, a FCUL disponibiliza espaço para um debate sobre o futuro dos conteúdos digitais, a necessária mercantilização&nbsp; de que os consumidores têm estado anos à espera e a re-credibilização de uma indústria que pouco mais tem feito do que antagonizar os seus próprios clientes.<br><br>&nbsp;Trata-se de um encontro pró-digital, pró-Internet e pró-modernidade para o qual está confirmada a presença das seguintes individualidades:</p><ul><li>Mário Jorge Silva – professor universitário no IST</li><li>Pedro Ramalho Carlos – empreendedor e ex-gestor de operador de comunicações</li><li>José Valverde – presidente da AGEFE</li><li>Maria João Nogueira – blogger, comunicadora</li><li>José Magalhães - deputado</li><li>Michael Seufert - deputado</li><li>Gustavo Homem – empreendedor e ex-dirigente associativo do sector das TIC</li><li>Rui Seabra – presidente da ANSOL</li></ul><p>A discussão, moderada por Pedro Veiga (professor catedrático na FCUL), estará centrada nas soluções para um mercado de conteúdos moderno, soluções que permitam a aquisição prática e flexível de conteúdos protegidos por direito de autor, sem onerar dispositivos de armazenamento que em muitos casos alojam apenas dados de trabalho ou simplesmente dado pertencentes ao seu proprietário.<br><br>O evento pretende apontar soluções para sustentabilidade do mercado digital compatíveis com o respeito pelos consumidores.<br><br>A entrada é livre e apenas limitada pela capacidade do anfiteatro (180 lugares).</p>
