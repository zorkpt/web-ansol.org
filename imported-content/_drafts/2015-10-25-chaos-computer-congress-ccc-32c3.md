---
categories: []
metadata:
  event_location:
  - event_location_value: CCH Congress Center Hamburg, Germany, Earth, Milky Way
  event_site:
  - event_site_url: https://events.ccc.de/congress/
    event_site_title: Chaos Computer Congress CCC 32c3
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-12-27 00:00:00.000000000 +00:00
    event_start_value2: 2015-12-30 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 130
  - tags_tid: 111
  - tags_tid: 41
  - tags_tid: 112
  node_id: 366
layout: evento
title: Chaos Computer Congress CCC 32c3
created: 1445734175
date: 2015-10-25
---
<p>The 32st Chaos Communication Congress (31C3) is an annual four-day conference on technology, society and utopia. The Congress offers&nbsp;<a href="https://events.ccc.de/congress/2014/wiki/Static:Schedule" title="Static:Schedule">lectures</a>&nbsp;and<a href="https://events.ccc.de/congress/2014/wiki/Static:Self-organized_Sessions" title="Static:Self-organized Sessions">workshops and various events</a>&nbsp;on a multitude of topics including (but not limited to) information technology and generally a critical-creative attitude towards technology and the discussion about the effects of technological advances on society.</p><p>For 32 years, the congress has been organized by the community and appreciates all kinds of participation. You are encouraged to contribute by&nbsp;<a href="https://events.ccc.de/congress/2014/wiki/Static:Volunteers" title="Static:Volunteers">volunteering</a>, setting up and hosting&nbsp;<a href="https://events.ccc.de/congress/2014/wiki/Static:Self-organized_Sessions" title="Static:Self-organized Sessions">hands-on and self-organized events</a>&nbsp;with the other components of your&nbsp;<a href="https://events.ccc.de/congress/2014/wiki/Static:Assemblies" title="Static:Assemblies">assembly</a>&nbsp;or presenting&nbsp;<a href="https://events.ccc.de/congress/2014/wiki/Static:Projects" title="Static:Projects">your own projects</a>&nbsp;to fellow hackers.</p><p>Updated information are covered by the<a href="http://events.ccc.de/" class="external text" rel="nofollow">CCC Events Blog</a>&nbsp;or via&nbsp;<a href="http://twitter.com/ccc" class="external text" rel="nofollow">Twitter (@CCC)</a>.<span style="font-size: 13.008px; line-height: 1.538em;">&nbsp;</span></p>
