---
categories: []
metadata:
  event_location:
  - event_location_value: Bucharest, Romania
  event_site:
  - event_site_url: https://ripe71.ripe.net/
    event_site_title: Ripe 71
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-16 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-20 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 364
layout: evento
title: RIPE 71
created: 1445733289
date: 2015-10-25
---
<p>A RIPE Meeting is a five-day event where Internet service providers, network operators and other interested parties from around the world gather to:</p><ul><li>Participate in discussions about the policies and procedures used by the RIPE NCC to allocate Internet number resources</li><li>Participate in the RIPE Working Group sessions to discuss current technical and policy issues</li><li>Share experiences, latest developments and best common practices</li><li>Develop their network of peers in the Internet community</li><li>Meet at lunches, coffee breaks and informal social events</li></ul><p>RIPE Meetings are open to everyone. They bring together people from different backgrounds, cultures, beliefs and genders. The RIPE community is unique and prides itself on providing&nbsp;<a href="https://www.ripe.net/ripe/meetings/ripe-meetings/enjoying-a-ripe-meeting" target="_blank" title="" class="ext-link ext-icon-1">a safe, supportive and respectful environment</a>.</p><p>If you can’t attend a RIPE Meeting in person, you can&nbsp;<a href="https://www.ripe.net/ripe/meetings/ripe-meetings/take-part-remotely" target="_blank" title="" class="ext-link ext-icon-1">participate remotely</a>.</p><p>For more information about RIPE Meetings please see the&nbsp;<a href="https://www.ripe.net/ripe/meetings/faq-meetings/ripe-meeting-faqs" target="_blank" title="" class="ext-link ext-icon-1">FAQs</a>.</p>
