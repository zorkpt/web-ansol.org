---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 59
layout: page
title: Agradecimentos
created: 1334966634
date: 2012-04-21
---
<p>Agradecemos às seguintes entidades pelo seu contributo para o funcionamento da ANSOL:</p><ul><li>à FSF, FSF France e FSF Europe (a quem somos associados) por nos inspirarem</li><li>à <a href="http://www.netureza.pt/">Netureza</a>, pelo <em>hosting</em> da nossa presença na Internet</li></ul>
