---
categories: []
metadata:
  event_site:
  - event_site_url: https://oslo.gonogo.live/accounts/login/?next=/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-10-12 23:00:00.000000000 +01:00
    event_start_value2: 2020-10-16 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 755
layout: evento
title: openSUSE + LibreOffice Virtual Conference
created: 1602002465
date: 2020-10-06
---

