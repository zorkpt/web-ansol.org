---
categories:
- open data
- porto
- transparência hackaday
- encontro
metadata:
  event_location:
  - event_location_value: Pavilhão Jardim do UPTEC PINC, Praça do Coronel Pacheco,
      Porto
  event_site:
  - event_site_url: http://www.transparenciahackday.org/2018/04/date-with-data-26-parlamento/
    event_site_title: 'Date With Data #26: Parlamento'
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-12 09:00:00.000000000 +01:00
    event_start_value2: 2018-05-12 16:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 85
  - tags_tid: 142
  - tags_tid: 291
  - tags_tid: 74
  node_id: 606
layout: evento
title: 'Date With Data #26: Parlamento'
created: 1523654572
date: 2018-04-13
---
<div class="row text-center"><p>Recebemos uma boa novidade: o <a href="https://parlamento.pt">website do Parlamento</a> foi renovado, incluindo agora uma secção de dados abertos! A 14 de Abril vamos aproveitar a deixa para nos voltarmos a debruçar sobre a Assembleia da República, que foi precisamente o tema do primeiro Date With Data em 2010.</p><p>A ideia será juntarmo-nos para olhar para o que já foi feito à volta do Parlamento e o que falta fazer, e começarmos a sujar as mãos com estes novos conjuntos de dados. Uma das tarefas que tomaremos em mãos será olhar para os dados e pensar as melhores formas de os re-converter para formatos mais fáceis de trabalhar. Vamos atirar para o ar chavões como XML, JSON, SQL e desmistificar os termos técnicos para tratarmos de coisas úteis. E olharemos de perto para o que o Parlamento agora nos dá, para descobrirmos o que agora é possível fazer e o que falta.</p><p>Como habitualmente, vamos também olhar para outros projetos e ideias, e esperar que a chuva não esteja demasiado presente. De qualquer maneira, espera-nos um dia no quentinho do Pavilhão Jardim a congeminar em conjunto, com a boa disposição do costume.</p><p>Ainda no rescaldo do Open Data Day, a Open Knowledge publicou um <a href="https://blog.okfn.org/2018/03/28/open-data-day-2018-getting-the-local-communities-in-porto-and-helsinki-to-talk-about-open-mapping/">artigo</a> sobre o evento no Porto e Helsínquia, que escrevemos em conjunto com os nossos colegas finlandeses. Publicámos também um <a href="http://www.transparenciahackday.org/2018/04/odd-2018-como-foi/">relato</a> em português com mais pormenores e links para os slides das apresentações.</p><p>E não te esqueças do portátil! Se tiveres à mão traz também uma ficha tripla para assegurarmos que toda a gente tem bateria.</p><p>Das 10:00 às 17:00, 14 de Abril, Sábado, no <a href="http://www.openstreetmap.org/?mlat=41.15137&amp;mlon=-8.61555#map=19/41.15138/-8.61555" target="_blank" rel="noopener" data-cke-saved-href="http://www.openstreetmap.org/?mlat=41.15137&amp;mlon=-8.61555#map=19/41.15138/-8.61555">Pavilhão Jardim do UPTEC PINC</a> (Praça Coronel Pacheco) — aparece e traz um amigo/a também!</p><p><em>Aponta já na agenda: o Date With Data do mês de Maio que será a 12 de Maio!</em></p></div>
