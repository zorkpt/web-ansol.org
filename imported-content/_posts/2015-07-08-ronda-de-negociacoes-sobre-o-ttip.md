---
categories: []
metadata:
  event_location:
  - event_location_value: Bruxelas
  event_site:
  - event_site_url: http://www.politico.eu/article/european-parliament-green-lights-key-ttip-compromise-trade-deal/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-07-12 23:00:00.000000000 +01:00
    event_start_value2: 2015-07-16 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 333
layout: evento
title: Ronda de negociações sobre o TTIP
created: 1436392658
date: 2015-07-08
---

