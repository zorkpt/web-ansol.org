---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://pt.wikimedia.org/wiki/WikidataDays_Sessions_II
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-04-25 15:00:00.000000000 +01:00
    event_start_value2: 2021-04-25 15:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 793
layout: evento
title: WikidataDays Sessions II
created: 1619015716
date: 2021-04-21
---
<p>A segunda edição dos WikidataDays Sessions será completamente online e dedicada à democracia e partidos políticos em Portugal. Tendo como base de trabalho uma listagem de todos os partidos políticos fundados e extintos em Portugal, iremos atualizar os dados no Wikidata e na Wikipédia, corrigir a informação que necessite ser corrigida assim como analisar as fontes de informação que podem ser usadas.</p>
