---
categories:
- software livre
- cerveja
- vagabundos
metadata:
  event_location:
  - event_location_value: Jedovnice, Moravia, Czech Republic
  event_site:
  - event_site_url: https://linuxbierwanderung.com/
    event_site_title: linux bier wanderung
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-08-24 23:00:00.000000000 +01:00
    event_start_value2: 2018-09-01 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 41
  - tags_tid: 239
  - tags_tid: 240
  node_id: 565
layout: evento
title: LBW 2018 - Vagabundos do Linux e cerveja 2018
created: 1522845113
date: 2018-04-04
---
<p>LBW is a week-long event which takes place in a different European country each summer, drawing together Open Source software enthusiasts from more than a dozen different countries, for a combination of talks, presentations, hands-on mini-projects, outdoor exercise, and good food and drink.</p><p><a href="https://linuxbierwanderung.com/">https://linuxbierwanderung.com/</a></p><h2>LBW 2018</h2><p>LBW in 2018 will take place from August 25th to September 2nd in Jedovnice, Moravia, Czech Republic. Jedovnice is a small town in the Moravian Karst - beatiful landscape full of caves. Weather at the end of August is good - not so hot and rainy as in July, and still quite warm. Last years temperatures were 20-30 °C max, and 10-20 °C min, with only one day of rain. More details on further pages.</p><h2>Registration</h2><p>If you plan to come, please register here: <a href="https://reg.linuxbierwanderung.com/13/">https://reg.linuxbierwanderung.com/13/</a></p>
