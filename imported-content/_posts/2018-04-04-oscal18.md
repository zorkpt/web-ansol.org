---
categories:
- open source
- open culture
- free software
- meeting
- albania
metadata:
  event_location:
  - event_location_value: TIRANA, ALBANIA
  event_site:
  - event_site_url: https://oscal.openlabs.cc/
    event_site_title: OSCAL'18
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-18 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-19 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 127
  - tags_tid: 265
  - tags_tid: 122
  - tags_tid: 129
  - tags_tid: 266
  node_id: 581
layout: evento
title: OSCAL'18
created: 1522862993
date: 2018-04-04
---
<p>5th edition OPEN SOURCE CONFERENCE ALBANIA TIRANA, ALBANIA / 19 &amp; 20 MAY 2018<br><br>OSCAL is the annual international Open Source Free Software Conference in Albania dedicated to empowering Software Freedom, Open Knowledge, Free Culture and Decentralization.</p><p>This year's OSCAL is expected to gather over 400 enthusiasts of open source technologies, developers, agencies and professionals of free/libre, open source culture troublemakers, cryptoanarchists and dreamers of a decentralised world. The participants will gather with a common belief in a world where information is open, free and decentralized. The conference not only provides everyone with the opportunity to share their knowledge and experience with the audience, but also inspires many others to act upon.</p><p>A wide variety of intriguing presentations and workshops will be held on both days of the conference. More specifically, each day from 9AM to 6PM, participants will have the chance to become acquainted with diﬀerent topics, as well as explore contribution and work opportunities. In addition, community infobooths will be present on both days. Mozilla, Fedora, LibreOﬀice, OpenStreetMap, Wikipedia, phpList, Nextcloud are only a few of the projects that OSCAL participants can get to know better.</p><p>OSCAL 2017 concluded with 70 presentations, speakers from 20 diﬀerent countries of the world and more than 300 attendants inspired to advance the free/libre open source movement even further. The best part were all the positive comments, that motivate the organizers to come up with something even more expanded. Everyone who wishes to become part of OSCAL 2018 may reserve their ticket, free of charge, by visiting oscal.openlabs.cc. Any financial contribution towards the development of the conference is certainly more than welcome.<br>The theme of OSCAL 2018 is "Open By Default", which reflects the main message of the organising team. #OpenByDefault is an idea to protest the normalization of closed platforms and silos and cultivate the idea that they should not define the status quo. In the past decades we have seen open sourcedo more good than any proprietary equivalent, so why do we still consider open source a perk? Software, Spaces and Processes should be open. Everything not meeting this criteria should be criticized &amp; retaliated, not normalized.</p><p><br>OSCAL is organized by Open Labs Hackerspace, the local community dedicated to empower<br>free/libre open source culture in Albania since 2012.<br>Further information:<br>Website: oscal.openlabs.cc<br>E-mail: oscal@openlabs.cc<br>Social networks:<br>Hashtag: #OSCAL2018<br>Twitter: @OSCALconf<br>Instagram: @oscalconf<br>Telegram channel: https://telegram.me/OSCAL2018<br>Important dates:<br>Call for Proposals: opens 11.01.2018 - deadline: open until 01.04.2018<br>Call for Sponsors: opens 30.11.2018 - deadline: open until 01.04.2018<br>Call for Volunteers: opens 08.03.2018 - deadline: open until 01.04.2018<br>Call for Info Booths: opens 17.01.2018 - deadline: open until 01.04.2018<br>Registration for the conference: opens 01.04.2018<br><br></p>
