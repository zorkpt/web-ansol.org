---
categories: []
metadata:
  node_id: 8
layout: page
title: Oops! Não encontrei isso...
created: 1332720300
date: 2012-03-26
---
<div class="content">
	<p>N&atilde;o encontrei o que procurava!</p>
	<ul>
		<li>
			Pode ser que esteja no nosso <a href='http://ate2012.ansol.org<?php print $_SERVER['REQUEST_URI']; ?>'>site anterior</a>...
		<li>
			Pode at&eacute; nem existir...</li>
	</ul>
</div>
