---
categories:
- ubuntu
- comunidade
- software livre
- ubucon
metadata:
  event_location:
  - event_location_value: " Antiguo Instituto, Xixón, Asturies, Spain"
  event_site:
  - event_site_url: http://ubucon.org/en/events/ubucon-europe/
    event_site_title: Ubucon 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-04-26 23:00:00.000000000 +01:00
    event_start_value2: 2018-04-28 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 162
  - tags_tid: 251
  - tags_tid: 41
  - tags_tid: 252
  node_id: 572
layout: evento
title: Ubucon Europe
created: 1522854303
date: 2018-04-04
---
<p>Encontro europeu da comunidade Ubuntu. Haverá palestras, encontros sociais e uma espicha asturiana (comes e besbes com cidra fresca...) durante os 3 dias do encontro.</p><p>Pretende ser um encontro não só de negócios, técnico mas principalmente que envolva a comunidade dos utilizadores developers e voluntários.</p>
