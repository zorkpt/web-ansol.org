---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://www.esop.pt/sessoes-de-sensibilizacao-software-open-source-para-a-administracao-publica/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-06-05 23:00:00.000000000 +01:00
    event_start_value2: 2013-06-05 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 166
layout: evento
title: Software Open Source para desktop e ferramentas de produtividade
created: 1369910662
date: 2013-05-30
---
<p>A&nbsp;<a href="http://www.ama.pt/">AMA</a>&nbsp;– Agência para a Modernização Administrativa e a&nbsp;<strong>ESOP</strong>&nbsp;– Associação de Empresas de Software Open Source Portuguesas, têm o prazer de convidar para um&nbsp;<strong>ciclo de sessões de sensibilização dedicadas à apresentação de soluções de Software Open Source para a Administração Pública</strong>.</p><p><strong>A primeira sessão terá lugar no dia 6 de Junho nas instalações do IHRU – Instituto da Habitação e da Reabilitação Urbana, em Lisboa e abordará a temática do Software Open Source para desktop e ferramentas de produtividade.</strong></p><div><strong>&nbsp;</strong></div><div>&nbsp;</div>
