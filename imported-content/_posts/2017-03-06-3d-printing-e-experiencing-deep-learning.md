---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.meetup.com/Big-Data-Developers-in-Lisbon/events/237672655/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-03-08 19:00:00.000000000 +00:00
    event_start_value2: 2017-03-08 21:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 491
layout: evento
title: 3D Printing e Experiencing Deep Learning
created: 1488834799
date: 2017-03-06
---
<p>Um evento com duas sessões: "3D Printing : A case study of Streaming Data Analytics", e "Experiencing Deep Learning".</p>
