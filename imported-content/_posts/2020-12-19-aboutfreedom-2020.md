---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://events.ccc.de/congress/2019/wiki/index.php/Assembly:About:freedom
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-12-27 00:00:00.000000000 +00:00
    event_start_value2: 2020-12-30 00:00:00.000000000 +00:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 769
layout: evento
title: about:freedom 2020
created: 1608392217
date: 2020-12-19
---
<p>about:freedom é o "canto" da Free Software Foundation Europe na CCC, este ano e pela primeira vez em formato online.</p>
