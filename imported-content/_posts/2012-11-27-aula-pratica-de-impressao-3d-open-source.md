---
categories: []
metadata:
  event_location:
  - event_location_value: Guimarães
  event_site:
  - event_site_url: http://lcd.guimaraes2012.pt/index.php?task=evt&id=88&option=com_lcd&lang=pt
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-12-20 00:00:00.000000000 +00:00
    event_start_value2: 2012-12-20 00:00:00.000000000 +00:00
  node_id: 109
layout: evento
title: Aula Prática de Impressão 3D Open Source
created: 1354045882
date: 2012-11-27
---
<p>A&nbsp;&nbsp;fabrica&ccedil;&atilde;o digital &eacute; uma realidade que hoje se encontra ao alcan&ccedil;e de todos, com a cria&ccedil;&atilde;o de projectos open source e outros que n&atilde;o o sendo se destinam ao mercado DIY (Do It Yourself - Fa&ccedil;a Voc&ecirc; Mesmo). Esta aula serve para introduzir os interessados ao mundo da fabrica&ccedil;&atilde;o&nbsp;digital e ao funcionamento de uma popular impressora 3D open source, a Makerbot.</p>
