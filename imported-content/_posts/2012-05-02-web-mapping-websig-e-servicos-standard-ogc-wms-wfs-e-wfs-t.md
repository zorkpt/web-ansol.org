---
categories: []
metadata:
  event_site:
  - event_site_url: http://www.faunalia.pt/cursowebmapping
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-05-27 23:00:00.000000000 +01:00
    event_start_value2: 2012-05-29 23:00:00.000000000 +01:00
  node_id: 61
layout: evento
title: 'Web Mapping (WebSIG) e serviços standard OGC: WMS, WFS e WFS-T'
created: 1335974647
date: 2012-05-02
---
<p>Web Mapping (WebSIG) e servi&ccedil;os standard OGC: WMS, WFS e WFS-T: 24<br />
	Horas/3 Dias - 28 a 30 Maio - &Eacute;vora<br />
	<br />
	Este curso permite aprender a usar um servidor de mapas, para poder<br />
	publicar os pr&oacute;prios dados geogr&aacute;ficos na Web. O curso tem a finalidade<br />
	de fornecer os conceitos fundamentais e a experi&ecirc;ncia pr&aacute;tica para<br />
	incorporar cartografia interactiva num s&iacute;tio Web.</p>
